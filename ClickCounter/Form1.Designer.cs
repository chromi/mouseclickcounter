﻿using System.Drawing;
namespace ClickCounter
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_counter1 = new System.Windows.Forms.Label();
            this.bt_start = new System.Windows.Forms.Button();
            this.bt_stop = new System.Windows.Forms.Button();
            this.bt_reset = new System.Windows.Forms.Button();
            this.pb_picture = new System.Windows.Forms.PictureBox();
            this.bt_load = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip_loadedImage = new System.Windows.Forms.ToolStripStatusLabel();
            this.gb_clickCounters = new System.Windows.Forms.GroupBox();
            this.rb_counter1 = new SelectionButton();
            this.rb_counter3 = new SelectionButton();
            this.rb_counter2 = new SelectionButton();
            this.lb_counter2 = new System.Windows.Forms.Label();
            this.lb_counter3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pb_picture)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.gb_clickCounters.SuspendLayout();
            this.SuspendLayout();
            // 
            // lb_counter1
            // 
            this.lb_counter1.AutoSize = true;
            this.lb_counter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_counter1.Location = new System.Drawing.Point(124, 24);
            this.lb_counter1.Name = "lb_counter1";
            this.lb_counter1.Size = new System.Drawing.Size(16, 17);
            this.lb_counter1.TabIndex = 0;
            this.lb_counter1.Text = "0";
            // 
            // bt_start
            // 
            this.bt_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_start.Location = new System.Drawing.Point(12, 194);
            this.bt_start.Name = "bt_start";
            this.bt_start.Size = new System.Drawing.Size(124, 35);
            this.bt_start.TabIndex = 2;
            this.bt_start.Text = "Start counting";
            this.bt_start.UseVisualStyleBackColor = true;
            this.bt_start.Click += new System.EventHandler(this.bt_start_Click);
            // 
            // bt_stop
            // 
            this.bt_stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_stop.Location = new System.Drawing.Point(12, 235);
            this.bt_stop.Name = "bt_stop";
            this.bt_stop.Size = new System.Drawing.Size(124, 35);
            this.bt_stop.TabIndex = 3;
            this.bt_stop.Text = "Stop";
            this.bt_stop.UseVisualStyleBackColor = true;
            this.bt_stop.Click += new System.EventHandler(this.bt_stop_Click);
            // 
            // bt_reset
            // 
            this.bt_reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_reset.Location = new System.Drawing.Point(12, 276);
            this.bt_reset.Name = "bt_reset";
            this.bt_reset.Size = new System.Drawing.Size(124, 35);
            this.bt_reset.TabIndex = 4;
            this.bt_reset.Text = "Reset";
            this.bt_reset.UseVisualStyleBackColor = true;
            this.bt_reset.Click += new System.EventHandler(this.bt_reset_Click);
            // 
            // pb_picture
            // 
            this.pb_picture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_picture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_picture.Location = new System.Drawing.Point(185, 20);
            this.pb_picture.Name = "pb_picture";
            this.pb_picture.Size = new System.Drawing.Size(521, 297);
            this.pb_picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_picture.TabIndex = 5;
            this.pb_picture.TabStop = false;
            this.pb_picture.Click += new System.EventHandler(this.pb_picture_Click);
            // 
            // bt_load
            // 
            this.bt_load.Location = new System.Drawing.Point(12, 12);
            this.bt_load.Name = "bt_load";
            this.bt_load.Size = new System.Drawing.Size(124, 35);
            this.bt_load.TabIndex = 6;
            this.bt_load.Text = "Load image";
            this.bt_load.UseVisualStyleBackColor = true;
            this.bt_load.Click += new System.EventHandler(this.bt_load_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStrip_loadedImage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 320);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(718, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // toolStrip_loadedImage
            // 
            this.toolStrip_loadedImage.Name = "toolStrip_loadedImage";
            this.toolStrip_loadedImage.Size = new System.Drawing.Size(98, 17);
            this.toolStrip_loadedImage.Text = "No image loaded";
            // 
            // gb_clickCounters
            // 
            this.gb_clickCounters.Controls.Add(this.panel3);
            this.gb_clickCounters.Controls.Add(this.panel2);
            this.gb_clickCounters.Controls.Add(this.panel1);
            this.gb_clickCounters.Controls.Add(this.lb_counter3);
            this.gb_clickCounters.Controls.Add(this.lb_counter2);
            this.gb_clickCounters.Controls.Add(this.rb_counter2);
            this.gb_clickCounters.Controls.Add(this.rb_counter3);
            this.gb_clickCounters.Controls.Add(this.rb_counter1);
            this.gb_clickCounters.Controls.Add(this.lb_counter1);
            this.gb_clickCounters.Location = new System.Drawing.Point(12, 53);
            this.gb_clickCounters.Name = "gb_clickCounters";
            this.gb_clickCounters.Size = new System.Drawing.Size(167, 135);
            this.gb_clickCounters.TabIndex = 8;
            this.gb_clickCounters.TabStop = false;
            this.gb_clickCounters.Text = "Counters";
            // 
            // rb_counter1
            // 
            this.rb_counter1.AutoSize = true;
            this.rb_counter1.Checked = true;
            this.rb_counter1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_counter1.Location = new System.Drawing.Point(6, 24);
            this.rb_counter1.Name = "rb_counter1";
            this.rb_counter1.Size = new System.Drawing.Size(88, 21);
            this.rb_counter1.TabIndex = 2;
            this.rb_counter1.TabStop = true;
            this.rb_counter1.Text = "Counter 1";
            this.rb_counter1.UseVisualStyleBackColor = true;
            this.rb_counter1.setCountingLabel(this.lb_counter1);
            this.rb_counter1.setColorPanel(this.panel1);
            this.rb_counter1.setcorrespondingColor(Color.Red);
            // 
            // rb_counter3
            // 
            this.rb_counter3.AutoSize = true;
            this.rb_counter3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_counter3.Location = new System.Drawing.Point(6, 96);
            this.rb_counter3.Name = "rb_counter3";
            this.rb_counter3.Size = new System.Drawing.Size(88, 21);
            this.rb_counter3.TabIndex = 3;
            this.rb_counter3.Text = "Counter 3";
            this.rb_counter3.UseVisualStyleBackColor = true;
            this.rb_counter3.setCountingLabel(this.lb_counter3);
            this.rb_counter3.setColorPanel(this.panel3);
            this.rb_counter3.setcorrespondingColor(Color.Blue);
            // 
            // rb_counter2
            // 
            this.rb_counter2.AutoSize = true;
            this.rb_counter2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_counter2.Location = new System.Drawing.Point(6, 58);
            this.rb_counter2.Name = "rb_counter2";
            this.rb_counter2.Size = new System.Drawing.Size(88, 21);
            this.rb_counter2.TabIndex = 4;
            this.rb_counter2.Text = "Counter 2";
            this.rb_counter2.UseVisualStyleBackColor = true;
            this.rb_counter2.setCountingLabel(this.lb_counter2);
            this.rb_counter2.setColorPanel(this.panel2);
            this.rb_counter2.setcorrespondingColor(Color.Lime);
            // 
            // lb_counter2
            // 
            this.lb_counter2.AutoSize = true;
            this.lb_counter2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_counter2.Location = new System.Drawing.Point(124, 60);
            this.lb_counter2.Name = "lb_counter2";
            this.lb_counter2.Size = new System.Drawing.Size(16, 17);
            this.lb_counter2.TabIndex = 5;
            this.lb_counter2.Text = "0";
            // 
            // lb_counter3
            // 
            this.lb_counter3.AutoSize = true;
            this.lb_counter3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_counter3.Location = new System.Drawing.Point(124, 98);
            this.lb_counter3.Name = "lb_counter3";
            this.lb_counter3.Size = new System.Drawing.Size(16, 17);
            this.lb_counter3.TabIndex = 6;
            this.lb_counter3.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BackColor = this.rb_counter1.correspondingColor;
            this.panel1.Location = new System.Drawing.Point(100, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(13, 10);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.BackColor = this.rb_counter2.correspondingColor;
            this.panel2.Location = new System.Drawing.Point(100, 69);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(13, 10);
            this.panel2.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.BackColor = this.rb_counter3.correspondingColor;
            this.panel3.Location = new System.Drawing.Point(100, 107);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(13, 10);
            this.panel3.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 342);
            this.Controls.Add(this.gb_clickCounters);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.bt_load);
            this.Controls.Add(this.pb_picture);
            this.Controls.Add(this.bt_reset);
            this.Controls.Add(this.bt_stop);
            this.Controls.Add(this.bt_start);
            this.Name = "Form1";
            this.Text = "Mouse Click Counter";
            ((System.ComponentModel.ISupportInitialize)(this.pb_picture)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gb_clickCounters.ResumeLayout(false);
            this.gb_clickCounters.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_counter1;
        private System.Windows.Forms.Button bt_start;
        private System.Windows.Forms.Button bt_stop;
        private System.Windows.Forms.Button bt_reset;
        private System.Windows.Forms.PictureBox pb_picture;
        private System.Windows.Forms.Button bt_load;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripStatusLabel toolStrip_loadedImage;
        private System.Windows.Forms.GroupBox gb_clickCounters;
        private SelectionButton rb_counter2;
        private SelectionButton rb_counter3;
        private SelectionButton rb_counter1;
        private System.Windows.Forms.Label lb_counter3;
        private System.Windows.Forms.Label lb_counter2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
    }
}

