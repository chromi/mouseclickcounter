﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClickCounter
{
    public partial class Form1 : Form
    {
        private String pathToImage = null; 

        private Boolean isCounting = false;
        private Boolean isloadedImage = false; 

        public Form1()
        {
            InitializeComponent();
            this.TopLevel = true; 
        }

        private void bt_start_Click(object sender, EventArgs e)
        {
            this.isCounting = true; 
        }

        private void bt_stop_Click(object sender, EventArgs e)
        {
            this.isCounting = false; 
        }

        private void bt_reset_Click(object sender, EventArgs e)
        {
            SelectionButton selected = this.getSelectedRadioButton();
            selected.resetCountingLabel();    
        }

        private SelectionButton getSelectedRadioButton()
        {
            if (this.rb_counter1.Checked == true)
                return this.rb_counter1;
            if (this.rb_counter2.Checked == true)
                return this.rb_counter2;
            if (this.rb_counter3.Checked == true)
                return this.rb_counter3;

            return null; 
        }

        private void bt_load_Click(object sender, EventArgs e)
        {
            String path = this.getPathToImage();
            if (path == null)
            {
                MessageBox.Show("Invalid path to image file.", "Invalid path", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; 
            }

            this.pathToImage = path;
            Image i = Image.FromFile(path); 
            this.pb_picture.Image = i;

            this.toolStrip_loadedImage.Text = this.pathToImage;
            this.isloadedImage = true; 
        }

        private String getPathToImage()
        {
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String tmp = this.openFileDialog.FileName;
                if (tmp == null)
                    return null;
                return tmp; 
            }

            return null; 
        }

        private void pb_picture_Click(object sender, EventArgs e)
        {
            if (this.isloadedImage && this.isCounting)
            {
                MouseEventArgs me = (MouseEventArgs)e;
                Point coords = me.Location;

                this.drawPoint(coords, this.getSelectedColor());
                this.incClickCounter();
            }
        }

        private void drawPoint(Point coords, Color color)
        {
            Graphics g = Graphics.FromHwnd(this.pb_picture.Handle);
            SolidBrush brush = new SolidBrush(color);
            Point dPoint = coords; 
            dPoint.X = dPoint.X - 2;
            dPoint.Y = dPoint.Y - 2;
            Rectangle rect = new Rectangle(dPoint, new Size(4, 4));
            g.FillRectangle(brush, rect);
            g.Dispose();
        }

        private Color getSelectedColor()
        {
            SelectionButton tmp = this.getSelectedRadioButton();
            return tmp.getCorrespondingColor(); 
        }

        private void incClickCounter()
        {
            if (this.isCounting && this.isloadedImage)
            {
                this.getSelectedRadioButton().incCountingLabel(); 
            }
        }
    }
}
